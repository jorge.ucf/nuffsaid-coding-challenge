import csv


def load_csv():
   with open('school_data.csv', newline='',encoding='cp1252') as csvfile:
        data = {"NCESSCH":[], "LEAID":[], "LEANM05":[], "SCHNAM05":[], "LCITY05":[], "LSTATE05":[], "LATCOD":[], "LONCOD":[], "MLOCALE":[], "ULOCALE":[],"STATUS05":[]}
        csv_reader = csv.reader(csvfile, delimiter=',')
        next(csv_reader)
        for row in csv_reader:
           data.get("NCESSCH").append(row[0])
           data.get("LEAID").append(row[1])
           data.get("LEANM05").append(row[2])
           data.get("SCHNAM05").append(row[3])
           data.get("LCITY05").append(row[4])
           data.get("LSTATE05").append(row[5])
           data.get("LATCOD").append(row[6])
           data.get("LONCOD").append(row[7])
           data.get("MLOCALE").append(row[8])
           data.get("ULOCALE").append(row[9])
           data.get("STATUS05").append(row[10])   
   return data


def get_count(column):
    types = {}
    for entry in column:
        if types.get(entry) is None:
           types[entry] = 1
        else:
           types[entry] += 1
    return types


def get_max(column):
    max_column, max_instances = "", -1
    types = get_count(column)
    for entry in types:
       instances = types.get(entry)
       if instances > max_instances:
           max_column, max_instances = entry,instances 
    return max_column, max_instances


def get_unique_count(column):
    types = get_count(column)
    return len(types)


def print_counts():
    data = load_csv()
    print("Total Schools: "+str(len(data.get("NCESSCH"))))

    print("Schols by State:")
    states = get_count(data.get("LSTATE05"))
    for state in states:
       print(state+": "+str(states.get(state)))
    print("** State validation was not performed, state C is supposed to be DC it is invalid in line 18711")

    print("Schools by Metro-centric locale:")
    metros = get_count(data.get("MLOCALE"))    
    for metro in metros:
       print(metro+": "+str(metros.get(metro)))
 
    city, num_schools = get_max(data.get("LCITY05"))
    print("City with the most schools: "+city+" ("+str(num_schools)+" schools)")

    unique_schools = get_unique_count(data.get("LCITY05"))
    print("Unique cities with at least one school: "+str(unique_schools))


def main():
    print_counts()
    return(0)


if __name__ == '__main__':
    main()
