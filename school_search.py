import csv
import time


def load_csv():
   with open('school_data.csv', newline='',encoding='cp1252') as csvfile:
        data = {"NCESSCH":[], "LEAID":[], "LEANM05":[], "SCHNAM05":[], "LCITY05":[], "LSTATE05":[], "LATCOD":[], "LONCOD":[], "MLOCALE":[], "ULOCALE":[],"STATUS05":[]}
        csv_reader = csv.reader(csvfile, delimiter=',')
        next(csv_reader)
        for row in csv_reader:
           data.get("NCESSCH").append(row[0].upper())
           data.get("LEAID").append(row[1].upper())
           data.get("LEANM05").append(row[2].upper())
           data.get("SCHNAM05").append(row[3].upper())
           data.get("LCITY05").append(row[4].upper())
           data.get("LSTATE05").append(row[5].upper())
           data.get("LATCOD").append(row[6].upper())
           data.get("LONCOD").append(row[7].upper())
           data.get("MLOCALE").append(row[8].upper())
           data.get("ULOCALE").append(row[9].upper())
           data.get("STATUS05").append(row[10].upper())   
   return data


def get_locations(column):
    types = {}
    location = 0
    for entry in column:
        if types.get(entry) is None:
           types[entry] = []
        types.get(entry).append(location)
        location += 1 
    return types


def get_set(column):
    key_list = {}
    location = 0
    for entry in column:
       sub_entries = entry.split(' ')
       for sub_entry in sub_entries:
           if key_list.get(sub_entry) is None:
              key_list[sub_entry] = []
           if location not in key_list.get(sub_entry):
              key_list.get(sub_entry).append(location)
       location += 1
    return key_list


def search_schools(search_string):
    res = []
    result_matrix = {"SCHNAM05":[],"LCITY05":[],"LSTATE05":[]}
    search_keys = search_string.upper().split(' ')
    data = load_csv()
    max = len(data.get("NCESSCH"))
    state_range = [-max,max]
    city_range = [-max,max]
    name_range = [-max,max]
    all_range = [-max,max,"","",[]]
    # create a state index
    state_locations = get_locations(data.get("LSTATE05"))
    # create a city index
    city_locations = get_locations(data.get("LCITY05"))
    # create a set of school names 
    school_names = get_set(data.get("SCHNAM05"))

    # Perform Search
    start = time.time_ns()
    for search_key in search_keys:
        if state_locations.get(search_key) is not None:
            result_matrix.get("LSTATE05").extend(state_locations.get(search_key))
        if city_locations.get(search_key) is not None:
            result_matrix.get("LCITY05").extend(city_locations.get(search_key))
        if school_names.get(search_key) is not None:
            result_matrix.get("SCHNAM05").extend(school_names.get(search_key))
    if len(result_matrix.get("LSTATE05")) > 0:
       result_matrix.get("LSTATE05").sort()
       state_range = result_matrix.get("LSTATE05")[0], result_matrix.get("LSTATE05")[len(result_matrix.get("LSTATE05"))-1]
       if state_range[0] > all_range[0]:
          all_range[0] = state_range[0]
          all_range[2] = "LSTATE05"
       if state_range[1] < all_range[1]:
          all_range[1] = state_range[1]
          all_range[3] = "LSTATE05"
       all_range[4].append( "LSTATE05")
    if len(result_matrix.get("LCITY05")) > 0:
       result_matrix.get("LCITY05").sort()
       city_range = result_matrix.get("LCITY05")[0], result_matrix.get("LCITY05")[len(result_matrix.get("LCITY05"))-1]
       if city_range[0] > all_range[0]:
          all_range[0] = city_range[0]
          all_range[2] = "LCITY05"
       if city_range[1] < all_range[1]:
          all_range[1] = city_range[1]
          all_range[3] = "LCITY05"
       all_range[4].append( "LCITY05")
    if len(result_matrix.get("SCHNAM05")) > 0:
       result_matrix.get("SCHNAM05").sort()
       name_range = result_matrix.get("SCHNAM05")[0], result_matrix.get("SCHNAM05")[len(result_matrix.get("SCHNAM05"))-1]
       if name_range[0] > all_range[0]:
          all_range[0] = name_range[0]
          all_range[2] = "SCHNAM05"
       if name_range[1] < all_range[1]:
          all_range[1] = name_range[1]
          all_range[3] = "SCHNAM05"
       all_range[4].append("SCHNAM05")
    res = []
    valid_lists = all_range[4]
    valid_lists.remove(all_range[2])
    for main_item in result_matrix.get(all_range[2]):
       if len(valid_lists) > 0:
          next_list = valid_lists[0]
          valid_lists.remove(next_list)
          for sub_item in result_matrix.get(next_list):
             if main_item == sub_item:
                if len(valid_lists) > 0:
                   for sub_sub_item in result_matrix.get(valid_lists[0]):
                       if sub_item == sub_sub_item:
                          res.append(main_item)
                else:
                   res.append(main_item)
       else:
          res.append(main_item)
    if len(res) < 3:
       res.append(all_range[1])
       if len(res) < 3:
          res.append(all_range[1])
    results = [{"SCHNAM05":data.get("SCHNAM05")[res[0]],"LCITY05":data.get("LCITY05")[res[0]],"LSTATE05":data.get("LSTATE05")[res[0]]},
               {"SCHNAM05":data.get("SCHNAM05")[res[1]],"LCITY05":data.get("LCITY05")[res[1]],"LSTATE05":data.get("LSTATE05")[res[1]]},
               {"SCHNAM05":data.get("SCHNAM05")[res[2]],"LCITY05":data.get("LCITY05")[res[2]],"LSTATE05":data.get("LSTATE05")[res[2]]}]
    now = time.time_ns()
    search_time = ((now - start)/1000.0)

    # Print Search results
    print("Results for \""+search_string+"\" (search took: "+str(search_time)+"s)")
    index = 1
    for result in results:
        print(str(index)+". "+result.get("SCHNAM05"))
        print(result.get("LCITY05")+", "+result.get("LSTATE05"))
        index += 1


def main():
    print("Running all test cases.")
    search_schools("elementary school highland park")
    search_schools("jefferson belleville")
    search_schools("foley high alabama")
    search_schools("KUSKOKWIM")
    search_schools("ORANGEVILLE")
    return(0)


if __name__ == '__main__':
    main()
